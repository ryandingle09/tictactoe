INSTRUCTIONS TO RUN THIS APP -------

Please use linux OS or Mac OS

Tools-------------------------------
Python2.7
Virtualenv
Pip

Steps for 1st running the app:
1. Navigate to App root directory
2. Make sure you install virtualenv
3. Run on terminal `./startapp.sh`
4. Open browser and go to `http://localhost:8080'`

Step for re run the app:
1. Navigate to App root directory
2. Run on terminal `./rerun.sh`
3. Open browser and go to `http://localhost:8080'`


