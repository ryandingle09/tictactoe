#!/bin/bash

source env/bin/activate
./manage.py makemigrations game
./manage.py migrate
./manage.py runserver 0.0.0.0:8080