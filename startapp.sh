#!/bin/bash

virtualenv -p python2 env
source env/bin/activate
pip install -r requirements.txt
./manage.py makemigrations game
./manage.py migrate
./manage.py runserver 0.0.0.0:8080